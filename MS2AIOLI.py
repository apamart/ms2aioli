# Export Metashape to AIOLI
# Compatibility tested and improved with Metashape Pro 2.0.2
# Developed by AIOLI DEV TEAM (UMR 3495 MAP CNRS)
# Last revision on 10 october 2023
# MADE ? <= General improvement (split variable declaration and methods)
# MADE ? <= IMPROVE issue with missing exif in 35mm format (also missing in the GUI) with third-part library (from exif import Image ?) ou calculate with XY plane resolution...
# MADE <= SOLVE issue with zip export (read only file system only with MacOS ?)
# MADE ? <= ADD warning in case of missing EXIF
# IMPROVE exif read with DateTime
# MADE <= TEST with sclaled AND georeferenced dataset
# MADE <= TEST with multifocal/sensor in a chunk (several AutoCal created)
# MADE <= principal point is True to distort camera because AIOLI doesn't looks PP in ori files (for MicMac exporter, principal point has to be False)
# TEST with multichunk projects

import Metashape
import os, sys
import time
import subprocess
import re
import shutil
from distutils.util import strtobool
import threading

class Ms2Aioli:
    """Export class for convert Metashape project into Aioli project

    Attrs:
        zipping (bool): zipping or not ?
        chunk (Metashape.chunk): chunk to export.
        cameras (Metashape.camera): camera usable for export
        eq35 (string): value for equiv. 35mm
        reporting (bool): allows reporting or not ?
        metadating (bool) allows updating metadata or not ?
        time0 (float): for report time
        prefix (string): allows to avoid ITER_ITER_ issue
        prof (float): generic altitude
        name (string): final zip name
        save_path (string): folder path
        ori (string): Ori type
        oriBD (string): Ori name
        pathOri (string): Ori path
        pathMDT (string): MDT path
        pathImages (string): Images path
        scaling (int): scale for export
        exiftool (string): path to exiftool
        thumbs (bool): thumbs creation for images

    Methods:
        _self_path(self) : function used to find self path
        _parse_arg(self, args*): function used to parse manual arguments
        _to_folder(path) : function made to manage folder creation
        _check_bool(self, boolean, defaultboolean): function to manage boolean arguments
        _check_path(self, defaultpath, path): function to manage path arguments       
        _usable_cameras(chunk): cameras sorting to keep only those with an orientation in space
        _name_value(string): function used to manage zip name
        _path_value(save_path): check of export path, to see if it is given by function or manually by user
        _eq35_value(eq35): check of 35mm equivalent, to see if it is given by function or manually by user
        _scale_value(self, int): function used to scale scene if too small (camera frustrum issue)
        _report(strings_to_report): Report generation for keeping parameters
        _write(filepath, lines): File generation with list and writelines method
        _prefixed(camera, prefix): Camera name verification used to avoid ITER_ITER_ issue
        _MMbyPairFile(): creation of the 1st subfile .XML : MMbyPairFile produced by MicMac and re-used within Aioli as list of images
        _ORIExport(): creation of the 2nd subfile .TXT : OriExport MicMac's command used on viewer-side (Aioli to Potree)
        _OriFiles(): creation of 3rd subfile .XML : external orientation for each camera
        _FocEq35mm_metadata(): verifies EXIF TAG type (or given by user if missing) for the focal equiv. in 35mm and export 4rd subfiles .XML for metadata
        _InternalCalibration(): creation of the 5rd subfile .XML : internal calibration for each camera
        _Objects_export(): exports 2D and 3D objects => sparsecloud, densecloud and undistorded cameras
        go(): create every data needed for AIOLI, and zip it
    """
    ## CLASS VARIABLES ## 
    def __init__(\
            self, 
            chunk=None, 
            folder=None, 
            name = None, 
            eq35=None, 
            reporting=True, 
            metadating= True, 
            zipping=True, 
            scale=None, 
            exiftool= "D:/Users/flcadm\Desktop\SCRIPTS\PYTHON/n_Dame/nDame_0.4/tools/exiftool/exiftool.exe" #HERE PATH TO EXIFTOOL IF EXIFTOOL USED // None if exiftool is not used
            ):
        """Initialization of class with important parameters 

        Args:
            chunk (Metashape.chunk, optional): chunk to export. Defaults to active chunk.
            folder (string, optional): absolute path to AIOLI folder. Defaults to None.
            name (string, optional): name for zip creation. Defaults to "modele".
            reporting (bool, optional): allows reporting or not ? Defaults to True.
            metadating (bool) allows updating metadata or not ? Defaults to True.
            eq35 (string, optional): value for equiv. 35mm. Defaults to None.
            zipping (bool, optional): zipping or not ? Defaults to True.
            scaling (int, optional): scale factor for export. Defaults is 1.
            exiftool (string, optional): path to exiftool. 
        """
        if chunk is None :
            chunk = Metashape.app.document.chunk
        self.chunk = chunk # chunk exported
        self.cameras = self._usable_cameras(chunk) # cameras usable for export (with orientation)
        self.eq35 = self._eq35_value(eq35) # for eq35 parameter

        self.zipping = self._check_bool(self._parse_arg("-zip", "-z"), zipping) # allows zipping or not
        self.reporting = self._check_bool(self._parse_arg("-report", "-r"), reporting) # allows reporting or not
        self.metadating = self._check_bool(self._parse_arg("-meta", "-m"), metadating) # allows updating metadata or not
        self.scaling =  self._scale_value(scale) # allows scaling or not
        self.time0 = time.time() # for report time
        self.prefix = "ITER_" # allows to avoid ITER_ITER_ issue
        self.prof = 0 # generic altitude
        self.name = self._name_value(name) # name for zip
        self.exiftool = exiftool
        self.thumbs = False

        # directories for AIOLI elements
            # general folder
        self.save_path = self._path_value(folder)
            # OriBD folder 
        self.ori = "All/" # type of OriBD v
        self.oriBD = "Ori-{}".format(self.ori) # OriBD v
        self.pathOri = self._to_folder("{}/{}".format(self.save_path, self.oriBD)) # oriBD folder
            # MM-dir folder
        self.pathMDT = self._to_folder("{}/Tmp-MM-Dir/".format(self.save_path))
            # Images folder
        self.pathImages = self._to_folder("{}/Images/".format(self.save_path))
    #####################

    ## UTILS METHOD ##
    def _self_path(self):
        """Search parent path of this script

        Returns:
            string: return parent path
        """
        selfpath = os.path.realpath(os.path.dirname(__file__))
        selfpath.replace("\\", "/")
        return selfpath

    def _parse_arg(self, *args):
        """generic function to parse sys.argv

        Returns:
            string : argument found if exists. else, return none
        """
        # assign none to argument
        good_arg = None
        # if there is some argument (except arg[0] for script name)
        if len(sys.argv) > 1:
            # for each argument
            for i in range(len(sys.argv)):
                # for each potential name
                for arg in args :
                    if sys.argv[i] == arg :
                        # argument value is the following arg
                        good_arg = sys.argv[i+1]
        return good_arg

    def _to_folder(self, path):
        """function made to manage folder creation

        Args:
            path (string): path to check

        Returns:
            string : path chosen or none if path is invalid
        """
        try :
            # check if path exists or not
            if not os.path.exists(path):
                #if not, create
                os.makedirs(path)
            # return path
            return path
        except : 
            return 

    def _check_bool(self, boolean, given_parameter):
        """function made for checking booleans

        Args:
            boolean (bool or none): check if bool is pass in arguments
            given_parameter (bool): parameter given manually

        Returns:
            bool : good boolean 
        """
        # given parameter
        good_bool = given_parameter
        # if sys.arg give a usable boolean
        if boolean != None :
            good_bool = strtobool(boolean)
        # return resulting bool
        return good_bool

    def _check_path(self, defaultValue, given_path):
        """function made for checking paths

        Args:
            defaultValue (string or none): default value if paths doesn't exist
            given_path (string): path given

        Returns:
            string or none : path returned. None if doesn't exist
        """
        good_path = given_path
        if os.path.exists(good_path):
            return good_path
        else:
            return defaultValue

    def _usable_cameras(self, chunk):
        """cameras sorting to keep only those with an orientation in space

        Args:
            chunk (Metashape.chunk instance): used to retrieve cameras with space orientation 

        Returns:
            list : cameras usable by aioli
        """
        # list cam initialization
        cams=[]

        # for each camera in the chunk
        for camera in chunk.cameras:
            if camera.transform :
                cams.append(camera)

        # return only ones with space orientation. Others will not be used
        return cams

    def _name_value(self, name):
        """function made to check name from argument

        Args:
            name (string): name for zip creation.

        Returns:
            good_name: name chosen
        """
        # name from argument
        good_name = self._parse_arg("-name", "-n")
        # if not in argument, name is given or not in parameters
        if good_name == None or type(good_name) != str:
            if name :
                good_name = name
            else :
                good_name = Metashape.app.getString("Specify the output filename :")
        # stop if name not given finally in directory
        if good_name == None or good_name == "":
            good_name = "GenericName"
        # return good name
        return good_name

    def _path_value(self, save_path):
        """check of export path, to see if it is given by function or manually by user

        Args:
            save_path (string): export path for AIOLI folder or None if used like a standalone

        Returns:
            string: export path for AIOLI folder
        """
        # path from argument
        print(save_path)
        path = self._parse_arg("-apath", "-ap")
        # if not in argument, save_path is given or not in parameters
        if path == None :
            if save_path :
                path = save_path 
            else :
                path = Metashape.app.getExistingDirectory("Specify the output folder for the AIOLI folder :")
        complete_path = "{}/{}_AIOLI".format(path, self.name)
        complete_path = self._to_folder(complete_path)
        # stop if path not given finally in directory
        if complete_path == "/{}_AIOLI".format(self.name):
            Metashape.app.messageBox("Operation cancelled : path not given")
            return
        #return path
        print(complete_path)
        return complete_path

    def _eq35_value(self, eq35):
        """check of 35mm equivalent, to see if it is given by function or manually by user

        Args:
            eq35 (string): 35mm equivalent. Must be a digit.

        Returns:
            string: 35mm equivalent
        """
        # None eq35 before assigment
        eq = self._parse_arg("-eq35", "-e")
        # if eq35 is given or not in parameters
        if eq == None :
            if eq35 :
                eq = eq35
            else :
                eq = Metashape.app.getString("Specify the 35mm equivalent :", "50")

        # stop if eq35 not a digit
        if eq == None or eq.isdigit() == False:
            Metashape.app.messageBox("Operation cancelled : 35mm equivalent is not a digit")
            return
        #return eq35
        return eq

    def _scale_value(self, scale):

        def is_float(string):
            if string.replace(".", "").isnumeric():
                return True
            else:
                return False
            
        # Scale before assigment
        scaleArg = self._parse_arg("-scale", "-s")
        # if eq35 is given or not in parameters
        if scaleArg == None :
            if scale :
                scaleArg = scale
            else :
                scaleArg = Metashape.app.getString("Specify the scale :", "1")

        # stop if eq35 not a digit
        if scaleArg == None or is_float(scaleArg) == False:
            Metashape.app.messageBox("scale not given : it will be 1")
            scaleArg = "1"
        #return eq35
        return scaleArg

    def _report(self, strings_to_report):
        """Report generation for keeping parameters

        Args:
            string_to_report (string): string reported
        """
        # path to report
        report = "{}/export_report.log".format(self.save_path)

        # write report
        if self.reporting :
            with open(report, 'a') as f:
                f.writelines('\n' .join(strings_to_report))

    def _write(self, filepath, lines):
        """File generation with list and writelines method
        Args:
            filepath (string): file path for write informations
            lines (list): list of each line 
        """
        # write file
        if len(lines) > 0:
            with open(filepath, "a") as f:
                f.writelines("\n" .join(lines))

    def _prefixed(self, camera, prefix):
        """Camera name verification used to avoid ITER_ITER_ issue

        Args:
            camera (Metashape.Camera): camera verified
            prefix (string): characters used at the beginning of camera.label

        Returns:
            string: prefix used after verification => "" if ITER_ exists, "ITER_" if doesn't exist
        """
        # verification
        good_prefix = "" if camera.label.startswith(prefix) else prefix
        # return good prefix
        return good_prefix
    ###################

    ## EXPORT PHASES METHOD ##
    # This section simulates the result of MMbyPairFile produced by MicMac and re-used within Aioli as list of images
    def _MMbyPairFile(self):
        """Phase 1 : creation of the 1st subfile .XML : MMbyPairFile produced by MicMac and re-used within Aioli as list of images
        """
        # XML creation for each image
        MMbyPairFile = "{}/MMByPairFiles.xml".format(self.save_path)
        lines = []
        report = []

        lines.append("<?xml version=\"1.0\" ?>")
        lines.append("<ListOfName>")
        # for each usable camera
        for camera in self.cameras:
            imgsource = camera
            # prefix check to avoid ITER_ITER_ 
            prefix = self._prefixed(imgsource, self.prefix) # to avoid ITER_ITER_ issue
            lines.append("       <Name>{}{}.JPG</Name>".format(prefix, imgsource.label))
        lines.append("</ListOfName>")

        # report statistics
        report.append("MMbyPairFile started\n\nMMByPairFiles.xml created for {} cameras in the main folder.".format(len(self.cameras)))
        # report : not usable cameras 
        cameras_not_usable = [x for x in self.cameras + self.chunk.cameras if x not in self.cameras]

        # if there are some cameras not usable, or not
        if len(cameras_not_usable) != 0 :
            cameras_not_usable_label_list = []
            for camera in cameras_not_usable :
                cameras_not_usable_label_list.append(camera.label)
            report.append("MMbyPairFile didn't save : {}.".format(cameras_not_usable_label_list))
        else :
            report.append("MMbyPairFile saved every camera in chunk.\n\n")

        # write informations 
        self._write(MMbyPairFile, lines)
        # write report
        self._report(report)

    # This section simulates the result of OriExport MicMac's command used on viewer-side (Aioli to Potree)
    def _ORIExport(self):
        """Phase 2 : creation of the 2nd subfile .TXT : OriExport MicMac's command used on viewer-side (Aioli to Potree)
        """
        # creation of orientation file needed in OriExport
        OriExport = "{}/export.txt".format(self.save_path)
        lines = []
        report = []

        # for each usable camera
        for camera in self.cameras:
            imgsource = camera
            # This section to switch from local frame (relative) to chunk coordinates system
            T = self.chunk.transform.matrix
            cen_x = imgsource.center
            cen_y = T.mulp(cen_x)
            # Get XYZ
            estimated_xyz = self.chunk.crs.project(cen_y)
            # Get OPK
            m = self.chunk.crs.localframe(T.mulp(imgsource.center)) #transformation matrix to the LSE coordinates in the given point
            R = (m * T * imgsource.transform * Metashape.Matrix().Diag([1, -1, -1, 1])).rotation()
            estimated_opk = Metashape.utils.mat2opk(R) #estimated orientation angles - omega, phi, kappa
            # write OPK and XYZ informations
            prefix = self._prefixed(imgsource, self.prefix) # to avoid ITER_ITER_ issue
            lines.append("{}{}.JPG {} {} {} {} {} {}".format(prefix, imgsource.label, str(estimated_opk[0]),str(estimated_opk[1]),str(estimated_opk[2]), str(estimated_xyz[0]), str(estimated_xyz[1]), str(estimated_xyz[2])))

            # report : orientations by cameras
            report.append("For {} => XYZ position of camera center is / X : {:.2f} Y : {:.2f} Z : {:.2f} / OPK rotation of camera is / Omega : {:.2f} Phi : {:.2f} Kappa : {:.2f}."\
                .format(str(imgsource.label),\
                estimated_xyz[0], estimated_xyz[1], estimated_xyz[2],\
                estimated_opk[0], estimated_opk[1], estimated_opk[2]))
        # report statistics        
        report.append("Orientations created for {} cameras in export.txt.\n".format(len(self.cameras)))

        # write informations 
        self._write(OriExport, lines)
        # write report 
        self._report(report)

    # This section manages the creation of the Orientation and Metadata files
    def _OriFiles(self):
        """Phase 3 : creation of 3rd subfiles .XML : external orientation for each camera
        """
        def _ori_files_creation(self, camera, point_cloud, chunk, firstcam):
            """XML creation for camera orientation

            Args:
                camera (object): source camera used for the .xml
                point_cloud (object): sparse cloud used for tie projection
            """
            imgsource = camera
            # Export of camera external orientation
                # Rotation matrix below
            T = self.chunk.transform.matrix
                #C = imgsource.transform * Metashape.Matrix.diag((1, -1, -1, 1))# com Alexey :camera transformation matrix has been multiplied by 180-degree rotation matrix, since the direction of Z axis in camera system is inverted compared to the world coordinate system. but micmac have the same convention, so ok

            C = imgsource.transform

                # Compute camera center . no need of the photo.transform, used for transforming PIXEL location into chunk location.
            cen_p = imgsource.center
            cen_t = T.mulp(cen_p)
            cen_t_prj = self.chunk.crs.project(cen_t)
                #  chunk.transform returns the camera position/orientation in geocentric coordinate system, but it is also necessary to use crs.localframe() function that will allow to estimate orientation angles in the point related to the camera center.
            m = self.chunk.crs.localframe(cen_t)
            R = m * T * C
                # The rows of the rotational matrix should be normalized
            r1 = Metashape.Vector([R[0,0], R[0,1], R[0,2]])
            r2 = Metashape.Vector([R[1,0], R[1,1], R[1,2]])
            r3 = Metashape.Vector([R[2,0], R[2,1], R[2,2]])
            rot = Metashape.Matrix([r1.normalized(), r2.normalized(), r3.normalized()])
                # for computing angles : Metashape.utils.mat2opk(rB)

            # Creation of orientation file for each camera in MicMac's XML format
            prefix = self._prefixed(imgsource, self.prefix) # to avoid ITER_ITER_ issue
            oriFiles = "{}/Orientation-{}{}.JPG.xml".format(self.pathOri, prefix, imgsource.label)

            lines = []
            lines.append("<?xml version=\"1.0\" ?>")
            lines.append("<ExportAPERO>")
            lines.append("    <OrientationConique>")
            lines.append("        <OrIntImaM2C>")
            # All these tags are useless in this situation but are still mandatory for compliance with MicMac's convention
            lines.append("            <I00>0 0</I00>")
            lines.append("            <V10>1 0</V10>")
            lines.append("            <V01>0 1</V01>")
            lines.append("        </OrIntImaM2C>")
            lines.append("        <TypeProj>eProjStenope</TypeProj>")

            # Determine the calibration name. In micmac, the calibration file is named AutoCal_Foc + the focal in 1/1000 mm + _Cam- + camera model + .xml
            # The method below reuse AutoCal metho to fit with MicMac's output (let's hope it's robust enough for complex Sensor Name)
            foc = int(1000*imgsource.sensor.focal_length)
            model = imgsource.photo.meta["Exif/Model"]
            modelUnderscoretoEmpty = re.sub('_', '', model)
            modelSpacetoUnderscore = re.sub('\s+', '_', modelUnderscoretoEmpty)
            calib = camera.sensor
            sensors = chunk.sensors
            id = [i for i,x in enumerate(sensors) if x == calib]
            CalibName = "{}AutoCal_Foc-{}_Cam-{}_{}.xml".format(self.oriBD, str(foc), modelSpacetoUnderscore, id[0])

            lines.append("        <FileInterne>{}</FileInterne>".format(CalibName))
            lines.append("        <RelativeNameFI>true</RelativeNameFI>")
            lines.append("        <Externe>")
            # AltiSol (flight altitude) and Profondeur (english: depth) are redundant info which do not need to be accurate here
            # Below condition traceback to MS2MicMac script found on Agisoft's forum
            if firstcam==1:
                tiep_proj = point_cloud.projections[imgsource][0]
                XYZchunk = point_cloud.points[tiep_proj.track_id].coord # 4 colums instead of 3
                XYZchunk = Metashape.Vector([XYZchunk[0], XYZchunk[1], XYZchunk[2]]) #keep the 3 first columns)
                XYZground = T.mulp(XYZchunk)
                XYZground = self.chunk.crs.project(XYZground)
                self.prof = cen_t_prj[2] - XYZground[2]
            lines.append("            <AltiSol>{}</AltiSol>".format(str(self.prof)))
            lines.append("            <Profondeur>{}</Profondeur>".format(str(self.prof)))
            lines.append("            <Time>-1.00000000000000002e+30</Time>")
            lines.append("            <KnownConv>eConvApero_DistM2C</KnownConv>")
            # Camera position center
            lines.append("            <Centre>{} {} {}</Centre>".format(str(cen_t_prj[0]), str(cen_t_prj[1]), str(cen_t_prj[2])))
            lines.append("            <ParamRotation>")
            lines.append("                <CodageMatr>")
            # Rotation matrix
            lines.append("                    <L1>{} {} {}</L1>".format(str(rot[0, 0]), str(rot[0, 1]), str(rot[0, 2])))
            lines.append("                    <L2>{} {} {}</L2>".format(str(rot[1, 0]), str(rot[1, 1]), str(rot[1, 2])))
            lines.append("                    <L3>{} {} {}</L3>".format(str(rot[2, 0]), str(rot[2, 1]), str(rot[2, 2])))
            lines.append("                </CodageMatr>")
            lines.append("            </ParamRotation>")
            lines.append("        </Externe>")
            lines.append("        <ConvOri>")
            lines.append("            <KnownConv>eConvApero_DistM2C</KnownConv>")
            lines.append("        </ConvOri>")
            lines.append("    </OrientationConique>")
            lines.append("</ExportAPERO>")
            # write informations 
            self._write(oriFiles, lines)

        point_cloud = self.chunk.tie_points
        report = []

        i=0
        # for each camera usable
        for camera in self.cameras:
            i+=1
            metadata_copy_thread = self.Parallelization(_ori_files_creation, args=(self, camera, point_cloud, self.chunk, i))
            metadata_copy_thread.start_thread()            

        # report statistics
        report.append("\nOrientations created for {} cameras in {}.\n".format(len(self.cameras), self.oriBD))
        # write report 
        self._report(report)
    
    # This section verifies EXIF missing TAG focal equiv. in 35mm and exports associated metadata in MicMac XML convention for every camera in the chunk
    def _FocEq35mm_metadata(self):
        """Phase 4 : verifies EXIF TAG type (or given by user if missing) for the focal equiv. in 35mm and export 4rd subfiles .XML for metadata
        """

        def _OriXml_creation(self, imgsource, foc35) :
            """XML creation of params per camera

            Args:
                imgsource (object): source camera used for the .xml
                foc35 (string): focal equivalent at 35mm
            """
            # Creation of metadata file for each camera in MicMac's XML format
            prefix = self._prefixed(imgsource, self.prefix)
            MDTFiles = "{}{}{}.JPG-MDT-4227.xml".format(self.pathMDT, prefix, imgsource.label)

            lines = []
            lines.append("<?xml version=\"1.0\" ?>")
            lines.append("<XmlXifInfo>")
            lines.append("    <GITRev>MS2Aioli_2.0.py</GITRev>")
            lines.append("    <FocMM>{}</FocMM>".format(str(imgsource.photo.meta["Exif/FocalLength"])))
            lines.append("    <FocInMS>{}</FocInMS>".format(str(imgsource.calibration.f)))
            lines.append("    <!--Just to keep a trace from adjusted focal focal length calculated in Metashape (also in calibration file)-->")
            lines.append("    <Foc35>{}</Foc35>".format(str(foc35)))
            lines.append("    <ExpTime>{}</ExpTime>".format(str(imgsource.photo.meta["Exif/ExposureTime"])))
            lines.append("    <Diaph>{}</Diaph>".format(str(imgsource.photo.meta["Exif/FNumber"])))
            lines.append("    <IsoSpeed>{}</IsoSpeed>".format(str(imgsource.photo.meta["Exif/ISOSpeedRatings"])))
            # Or read Sz from Exif ? to prevent alteration from Metashape ?
            lines.append("    <Sz>{} {}</Sz>".format(str(imgsource.calibration.width), str(imgsource.calibration.height)))
            lines.append("    <Cam>{}</Cam>".format(re.sub('\s+', '_', str(imgsource.photo.meta["Exif/Model"]))))
            lines.append("    <BayPat>RVWB</BayPat>")
            lines.append("    <Date>")
            lines.append("        <Y>{}</Y>".format(str(imgsource.photo.meta["Exif/DateTimeOriginal"][0:4])))
            lines.append("        <M>{}</M>".format(str(imgsource.photo.meta["Exif/DateTimeOriginal"][5:7])))
            lines.append("        <D>{}</D>".format(str(imgsource.photo.meta["Exif/DateTimeOriginal"][8:10])))
            lines.append("            <Hour>")
            lines.append("                <H>{}</H>".format(str(imgsource.photo.meta["Exif/DateTimeOriginal"][11:13])))
            lines.append("                <M>{}</M>".format(str(imgsource.photo.meta["Exif/DateTimeOriginal"][14:16])))
            lines.append("                <S>{}</S>".format(str(imgsource.photo.meta["Exif/DateTimeOriginal"][17:19])))
            lines.append("            </Hour>")
            lines.append("    </Date>")
            lines.append("    <Orientation>{}</Orientation>".format(str(imgsource.orientation)))
            lines.append("    <!--Use Metashape convention e.g. (0 - unknown 1 - normal, 6 - 90 degree, 3 - 180 degree, 8 - 270 degree)-->")
            lines.append("</XmlXifInfo>")

            # write informations 
            self._write(MDTFiles, lines)

        report = []
        foc35Eq = []

        # for each camera usable
        for camera in self.cameras:
            imgsource = camera
            foc35 = self.eq35
            model = imgsource.photo.meta["Exif/Model"]
            giventype = "user"

            # if efl TAG value
            if str(imgsource.photo.meta["Exif/FocalLength35efl"]) != "None":
                foc35 = str(imgsource.photo.meta["Exif/FocalLength35efl"])
                giventype = "FocalLength35efl"
            # if 35mmFilm value
            elif str(imgsource.photo.meta["Exif/FocalLengthIn35mmFilm"]) != "None":
                foc35 = str(imgsource.photo.meta["Exif/FocalLengthIn35mmFilm"])
                giventype = "FocalLengthIn35mmFilm"
            # if 35mm Format value
            elif str(imgsource.photo.meta["Exif/FocalLengthIn35mmFormat"]) != "None":
                foc35 = str(imgsource.photo.meta["Exif/FocalLengthIn35mmFormat"])
                giventype = "FocalLengthIn35mmFormat"

            # report for each image
            warning = "WARNING" if giventype == "user" else ""
            report.append("{} => 35mm Equivalent Focal length : {} / given by {}. {}".format(imgsource.label, foc35, giventype, warning))
            report.append("sensor model is {}. Metadata are exported to /Tmp-MM-Dir folder.".format(str(model)))

            # list for each 35mm focal is recorded for foc35
            foc35Eq.append(foc35)

            metadata_copy_thread = self.Parallelization(_OriXml_creation, args=(self, imgsource, foc35))
            metadata_copy_thread.start_thread() 


        # report: 35mm nb and type recap
        nbtype = len(set(foc35Eq))
        report.append("\n{} types of 35mm Equivalent found for {} cameras | calibration exported for {} cameras to /Tmp-MM-Dir folder.\n"\
                      .format(nbtype, len(self.cameras), len(self.cameras)))

        # write report 
        self._report(report)

    # This section exports the internal calibration file in MicMac's XML convention for every camera in the chunk
    def _InternalCalibration(self):
        """Phase 5 : creation of the 5rd subfile .XML : internal calibration for each sensor
        """
        def _export_sensor_calibration(self, camera, chunk):
            """XML creation for Internal Calibration

            Args:
                camera (object): source camera used for the .xml
            """
            # APA wrote this new method below to fit with MicMac's output (let's hope it's robust enough for complex Sensor Name)
            calib = camera.sensor
            sensors = chunk.sensors
            id = [i for i,x in enumerate(sensors) if x == calib]
            foc = int(1000*calib.focal_length)
            PP1 = str(calib.calibration.width/2)
            PP2 = str(calib.calibration.height/2)   
            SzIm1 = str(calib.calibration.width)
            SzIm2 = str(calib.calibration.height)    
            model = camera.photo.meta["Exif/Model"]
            modelUnderscoretoEmpty = re.sub('_', '', model)
            modelSpacetoUnderscore = re.sub('\s+', '_', modelUnderscoretoEmpty)
            CalibName = "{}/AutoCal_Foc-{}_Cam-{}_{}.xml".format(self.pathOri, str(foc), modelSpacetoUnderscore, id[0])

            lines = []
            lines.append("<?xml version=\"1.0\" ?>")
            lines.append("<ExportAPERO>")
            lines.append("    <CalibrationInternConique>")
            lines.append("        <KnownConv>eConvApero_DistM2C</KnownConv>")
            lines.append("        <PP>{} {}</PP>".format(PP1, PP2))
            lines.append("        <F>{}</F>".format(str(calib.calibration.f)))
            lines.append("        <SzIm>{} {}</SzIm>".format(SzIm1, SzIm2))
            lines.append("        <CalibDistortion>")
            lines.append("            <ModRad>")
            lines.append("                <CDist>{} {}</CDist>".format(PP1,PP2))
            # set additionnal parameters of the calibration to 0, camera calibration must be refined in micmac on the basis of tie point and external ori
            lines.append("                <CoeffDist>0</CoeffDist>")
            lines.append("                <CoeffDist>0</CoeffDist>")
            lines.append("                <PPaEqPPs>true</PPaEqPPs>")
            lines.append("            </ModRad>")
            lines.append("        </CalibDistortion>")
            lines.append("    </CalibrationInternConique>")
            lines.append("</ExportAPERO>")

            # write informations 
            self._write(CalibName, lines)

        report = []

        # for each camera usable 
        sensor_lib = []
        for camera in self.cameras :
            sensor = camera.sensor.label + str(camera.sensor.key)
            if (sensor not in sensor_lib) :
                sensor_lib.append(sensor)
                metadata_copy_thread = self.Parallelization(_export_sensor_calibration, args=(self, camera, self.chunk))
                metadata_copy_thread.start_thread()

        # report statistics
        report.append("\ninternal calibration exported for {} cameras to {} folder.\n".format(len(self.cameras), self.oriBD))
        # write report 
        self._report(report)

    # This section exports the sparse / dense pointclouds, and undistorded cameras
    def _Objects_export(self):
        """Phase 6 : exports 2D and 3D objects => sparsecloud, densecloud and undistorded cameras
        """
        def _copy_metadata(camera, dst_path, all):
            """Copy metadata to exported undistorded images

            Args:
                src_path (Metashape.Camera): Source Camera
                dst_path (string): path to destination (exported unidstorded) cameras
            """
            try:
                # metadata transfer
                exiftoolpath = self.exiftool
                if exiftoolpath is not None :
                    src_path=camera.photo.path
                    if all == True :
                        exif = subprocess.Popen([exiftoolpath, "-TagsFromFile", src_path, "-exif:all", "--orientation", dst_path, "-overwrite_original"])
                        output, errors = exif.communicate()
                    else :
                        exif = subprocess.Popen([exiftoolpath, "-TagsFromFile", src_path, "-orientation", dst_path, "-overwrite_original"])
                        output, errors = exif.communicate()
                else :
                    report.append("\ncamera {} : metadata not transfered, exiftool not found.".format(camera.label))
            except:
                # if metadata is not found
                report.append("\nUndistorted images exported for {} cameras to /images.".format(len(self.cameras)))

        report = []

        self.chunk.exportPointCloud (
            path= "{}/C3DC_MicMac.ply".format(self.save_path),
            source_data=Metashape.DataSource.PointCloudData, 
            binary=True, 
            save_point_normal=True, 
            save_point_color=True, 
            save_point_classification=False, 
            save_point_confidence=False, 
            colors_rgb_8bit=True, 
            format=Metashape.PointCloudFormatPLY
        )
        self.chunk.exportPointCloud (
            path= "{}/AperiCloud_All.ply".format(self.save_path),
            source_data=Metashape.DataSource.TiePointsData, 
            binary=False, 
            save_point_normal=False, 
            save_point_color=True, 
            save_point_classification=False, 
            save_point_confidence=False, 
            colors_rgb_8bit=True, 
            format=Metashape.PointCloudFormatPLY
            )

        # Export of the image without distorsion
        ImageComp = Metashape.ImageCompression()
        ImageComp.jpeg_quality = 70

        for camera in self.cameras:
            img = camera.photo.image()
            calib = camera.sensor.calibration
            # This is the key line for colour correction. Here, I opted to square pixels and center principal point for avoid shifting in visualisation (Florent Comte).
            imgu = img.undistort(calib, True, True)
            prefix = self._prefixed(camera, self.prefix)
            imgupath = "{}{}{}.JPG".format(self.pathImages, prefix, camera.label)
            imgu.save(imgupath, ImageComp)
            if self.metadating:
                p = self.Parallelization(_copy_metadata, args=(camera, imgupath, True))
                p.start_thread()
            else :
                report.append("No metadata transfered from sources to undistorded images.")

            if self.thumbs :
                # Export of the thumbnails
                ThumbComp = Metashape.ImageCompression()
                ThumbComp.jpeg_quality = 30
                # subfolder thumbs
                thumb_dir = os.path.join(self.pathImages, 'thumbs')
                if not os.path.exists(thumb_dir):
                    os.mkdir(thumb_dir)
                # vignetting_size
                thumb_max_size = 200
                if imgu.width > imgu.height:
                    new_width = thumb_max_size
                    new_height = int(imgu.height * thumb_max_size / imgu.width)
                else:
                    new_height = thumb_max_size
                    new_width = int(imgu.width * thumb_max_size / imgu.height)
                # Image resizing
                thumb = imgu.resize(new_width, new_height)
                # Save in dedicated subfolder
                thumb_path = "{}/{}{}.JPG".format(thumb_dir, prefix, camera.label)
                thumb.save(thumb_path, ThumbComp)

        report.append("Apericloud and C3DC Pointclouds created and exported main folder.\n")
        self._report(report)
    
    ##########################

    ## MAIN FUNCTION TO EXPORT ##
    def go(self):
        """MAIN FUNCTION : create every data needed for AIOLI, and zip it
        """
        # function made to execut phase 
        def _phase_execut(phase):
            phase_func = phase[0]
            phase_name = phase[1]
            try:
                phase_func
                self._report(["\n-- {} exported without any problem--".format(phase_name)])
            except Exception as e:
                self._report(["\n-- {} export => ISSUE: {} --".format(phase_name,e)])
        
        # zip made
        def _make_archive(source, destination):
            base_name = '.'.join(destination.split('.')[:-1])
            format = destination.split('.')[-1]
            root_dir = os.path.dirname(source)
            base_dir = os.path.basename(source.strip(os.sep))
            shutil.make_archive(base_name, format, root_dir, base_dir)
        
        # if eq35 and path exists
        if self.eq35 != None and self.save_path != None and self.name != None :
            phases_list = [\
                    (self._MMbyPairFile(), "MMbyPairFiles.xml"),\
                    (self._ORIExport(), "ORIExport.xml"),\
                    (self._OriFiles(), "Orientation Files"),\
                    (self._FocEq35mm_metadata(), "Focal equiv. 35mm & metadata per camera"),\
                    (self._InternalCalibration(), "Internal calibration"),\
                    (self._Objects_export(), "Sparse / dense clouds and Images")\
                    ]
            try :
                for phase in phases_list :
                    _phase_execut(phase)
            except :
                #time for this phase
                time_complete = float(time.time() - self.time0)
                self._report(["\nAioli creation cancelled at {:.2F}.".format(time_complete)])
            else :
                #time for this phase
                time_complete = float(time.time() - self.time0)
                self._report(["\nAioli creation complete in {:.2F}.".format(time_complete)])
                if self.zipping:
                    zip_name = "{}.zip".format(self.save_path)
                    _make_archive(self.save_path, zip_name)
                    shutil.rmtree(self.save_path)
        # else, quit script
        else :
            print("export cancelled")
            pass
    #############################

    ######## PARALLELIZATION CLASS #########
    class Parallelization(threading.Thread):
        def __init__(self, func, args=()):
            super().__init__()
            self.func = func
            self.args = args

        def run(self):
            self.func(*self.args)

        def start_thread(self):
            self.start()
    ########################################

if __name__ == "__main__":
    Ms2Aioli().go()