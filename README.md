Release made with Metashape 2.0.2, accepting some changes (methods and variables, class, report, zip, metadata transfer, ...)
Possibility to launch the exporter with CTRL+R or "Tools => run script"
It will try to export the active chunk

-- Optional and non-optional elements --

There are some elements to give :
- a path
- a filename
- a 35mm equivalent (default is 50)
and some optionals :
- a report or not (default is true)
- a transfer of metadata or not (default is true)
- a zip phase of all elements or not (default is true)
- the exiftool path (.exe on windows)

-- How give them --

There are different ways to give these elements (Priority order is from top to bottom) :

- using arguments in run script box :
| -name or -n and after the filename (ex : -name CR204C)
| -path or -p and after the absolute path (ex : -path C:/path/to/folder (on windows))
| -eq35 or -e and after the 35mm equivalent (ex : -eq35 50)
| -report or -r and after the bool True/False or 1/0 (ex : -report 0)
| -meta or -m and after the bool True/False or 1/0 (ex : -transfer 0)
| -zip or -z and after the bool True/False or 1/0 (ex : -zip 0)

- using class parameter (if used from other script):
ex : 
from aioli_exporter import Ms2Aioli
Ms2Aioli(chunk=Metashape.app.document.chunk, folder=C:/path/to/folder, name=filename, eq35=50, reporting=True, metadating= True, zipping=True, exiftool=C:/path/to/exiftool.exe).go()

- using metashape poping boxes (optionals arguments are defaults, but you can use scripts arguments above):
| a string box for eq35 (need a float number)
| a string box for filename
| a directory search box for path

WARNING : 
Metadata are written with an external tool in .exe called exiftool. For now, you have to give the absolute path of exiftool in class init. If given path doesn't exists, the script considers exiftool like None and doesn't write metadata (avoid with metadating = 0 or False) 
